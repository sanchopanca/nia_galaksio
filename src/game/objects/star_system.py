from game.objects.space_object import SpaceObject
from game.objects.star import Star


class StarSystem(object):
    def __init__(self, star: Star, planets: list, comets: list,
                 asteroids: list, stations: list, sputniks: list):
        self.star = star
        self.planets = planets
        self.comets = comets
        self.asteroids = asteroids
        self.stations = stations
        self.sputniks = sputniks