from game.objects.space_object import SpaceObject
from game.objects.galaxy import Galaxy
from game.objects.planet import Planet
from game.objects.star import Star
from game.objects.star_system import StarSystem