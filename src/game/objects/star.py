from game.objects.space_object import SpaceObject


class Star(SpaceObject):
    def __init__(self, radius: float):
        SpaceObject.__init__(self)
        self.radius = radius