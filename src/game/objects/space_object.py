class SpaceObject(object):
    def __init__(self, static: bool=True, center_of_orbit: SpaceObject=None):
        self.static = static
        self.center_of_orbit = center_of_orbit