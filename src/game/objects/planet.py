from game.objects.space_object import SpaceObject


class Planet(SpaceObject):
    def __init__(self, star: SpaceObject, orbit_radius: float, satellites: list,
                 stations: list, sputniks: list):
        SpaceObject.__init__(self, False, star)
        self.satellites = satellites
        self.stations = stations
        self.sputniks = sputniks
        self.orbit_radius = orbit_radius