from game.objects import Planet, StarSystem, Galaxy, Star, SpaceObject

if __name__ == '__main__':
    sun = Star(0.1)
    earth = Planet(sun, orbit_radius=1.0, satellites=[], stations=[],
                   sputniks=[])
    solar_system = StarSystem(star=sun, planets=[earth], comets=[],
                              asteroids=[], stations=[], sputniks=[])
    galaxy = Galaxy(star_systems={solar_system: (0, 0, 0)})